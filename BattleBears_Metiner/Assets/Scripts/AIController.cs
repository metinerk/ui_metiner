﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : Unit {

    public float shootInterval;
    public float range;

    //A shootOffset variable so the AI doesn't shoot at the feet, but at the chest
    private Vector3 shootOffset = new Vector3(0, 1.5f, 0);

    private NavMeshAgent agent;

    //An enum allows us to create a variable type (in this case 'State')
    //This variable type can only have the values that we declare inside the enum
    private enum State
    {
        Idle,
        MovingToOutpost, 
        ChasingEnemy
    }

    private State currentState;

    private Outpost currentOutpost;
    private Unit currentEnemy;

    protected override void Start()
    {
        base.Start();
        agent = GetComponent<NavMeshAgent>();
        SetState(State.Idle);
    }

    private void SetState (State newState)
    {
        //This will stop all Coroutines on this MonoBehaviour (so only this object, not other ones)
        //We do this to ensure we are never in two states at the same time
        StopAllCoroutines();
        currentState = newState;
        switch (currentState)
        {
            case State.Idle:
                StartCoroutine(OnIdle());
                break;
            case State.MovingToOutpost:
                StartCoroutine(OnMovingToOutpost());
                break;
            case State.ChasingEnemy:
                StartCoroutine(OnChasingEnemy());
                break;
        }
    }

    IEnumerator OnIdle ()
    {
        //This is called once, when I enter the OnIdle Coroutine

        while (currentOutpost == null)
        {
            LookForOutpost();
            //yield return null pauses the execution of this method for one frame
            yield return null;

            //This is called every frame, until StopAllCoroutines is called.
        }

        SetState(State.MovingToOutpost);
    }

    IEnumerator OnMovingToOutpost()
    {
        //This is called once, when I enter the OnMovingToOutpost Coroutine
        //We set our target once, the outpost doesn't move so there is no need to recalculate the path
        agent.SetDestination(currentOutpost.transform.position);

        //As soon as the captureValue is 1 and the outpost belongs to my team, I move on
        while (!(currentOutpost.captureValue == 1 && currentOutpost.team == team))
        {
            LookForEnemy();
            //yield return null pauses the execution of this method for one frame
            yield return null;
            //This is called every frame, until StopAllCoroutines is called.
        }

        //When the outpost is fully captured, we can go back to Idle
        currentOutpost = null;
        SetState(State.Idle);
    }

    IEnumerator OnChasingEnemy()
    {
        float shootTimer = 0;

        while (currentEnemy.health > 0)
        {
            shootTimer += Time.deltaTime;
            //If our enemy is out of range, chase him down
            if (Vector3.Distance(transform.position, currentEnemy.transform.position) > range)
            {
                agent.SetDestination(currentEnemy.transform.position);
            } else
            {
                //We clear the current path, so the AI is not moving and shooting at the same time
                agent.ResetPath();

                if (shootTimer >= shootInterval)
                {
                    shootTimer = 0;
                    //We shoot at the enemies position, with a bit of offset so it shoots at the chest, not feet
                    ShootLasers(currentEnemy.transform.position + shootOffset, currentEnemy.transform);
                }
            }

            yield return null;
        }

        //When the enemy is dead, we go to Idle
        SetState(State.Idle);
    }

    void LookForEnemy ()
    {
        Collider[] surroundingColliders = Physics.OverlapSphere(transform.position, range);
        foreach(Collider c in surroundingColliders)
        {
            Unit otherUnit = c.GetComponent<Unit>();
            if (otherUnit != null && otherUnit.team != team && otherUnit.health > 0 
                && CanSee(otherUnit.transform.position + shootOffset, otherUnit.transform))
            {
                //Save the unit that we will start chasing
                currentEnemy = otherUnit;
                SetState(State.ChasingEnemy);
                //When we found our enemy, we stop looking
                return; //putting break; here does the same thing
            }
        }
    }

    void LookForOutpost ()
    {
        //Create a random index integer
        int r = Random.Range(0, GameManager.instance.outposts.Length);
        currentOutpost = GameManager.instance.outposts[r];
    }

    // Update is called once per frame
    void Update () {
        //PlayerController player = FindObjectOfType<PlayerController>();
        //agent.SetDestination(player.transform.position);

        //We send the current speed of the agent every frame, to display the movement animation
        anim.SetFloat("VerticalSpeed", agent.velocity.magnitude);
	}

    protected override void Die()
    {
        base.Die();
        StopAllCoroutines();
        //Stops the NavmeshAgent from moving
        agent.Stop();
        Destroy(GetComponent<Collider>());
    }
}
